package apiserver

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"serverApp/paydate"
	"testing"
	"time"
)

var (
	testData1 = paydate.YearlyPayDates{
		Month: 11,
		Days:  8,
	}
	testData2 = paydate.YearlyPayDates{
		Month: 12,
		Days:  36,
	}
)

func TestReturnNextPayDayHandle(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "http://localhost:8081/howMuch?pay_day=25", nil)
	w := httptest.NewRecorder()
	currentTime := time.Date(2022, time.Month(3), 17, 0, 0, 0, 0, time.UTC)
	handler := NewTimeHandler(currentTime)
	handler.returnNextPayDayHandle(w, req)
	res := w.Result()
	data, _ := ioutil.ReadAll(res.Body)
	if bytes.Compare(data, []byte("Days: 8")) == 0 {
		t.Errorf("expected Days: 8 got %v", string(data))
	}
}

func TestReturnAllPayDayHandle(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "http://localhost:8081/listHowMany?pay_day=25", nil)
	w := httptest.NewRecorder()
	currentTime := time.Date(2022, time.Month(11), 17, 0, 0, 0, 0, time.UTC)
	handler := NewTimeHandler(currentTime)
	handler.returnAllPayDaysHandle(w, req)
	res := w.Result()
	data, _ := ioutil.ReadAll(res.Body)
	var testData []paydate.YearlyPayDates
	testData = append(testData, testData1, testData2)
	th, _ := json.Marshal(testData)
	if bytes.Equal(data, th) {
		t.Errorf("got: %v, want: %v", string(data), string(th))
	}
}

func TestReturnNextPayDayHandleStatusOk(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "http://localhost:8081/howMuch?pay_day=25", nil)
	w := httptest.NewRecorder()
	currentTime := time.Date(2022, time.Month(3), 17, 0, 0, 0, 0, time.UTC)
	handler := NewTimeHandler(currentTime)
	handler.returnNextPayDayHandle(w, req)
	res := w.Result()
	if res.StatusCode != 200 {
		t.Errorf("expected status code 200 got %v", res.StatusCode)
	}
}

func TestReturnNextPayDayHandleStatusBadRequest(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "http://localhost:8081/howMuch?pay_day=40", nil)
	w := httptest.NewRecorder()
	currentTime := time.Date(2022, time.Month(3), 17, 0, 0, 0, 0, time.UTC)
	handler := NewTimeHandler(currentTime)
	handler.returnNextPayDayHandle(w, req)
	res := w.Result()
	if res.StatusCode != 400 {
		t.Errorf("expected status code 400 got %v", res.StatusCode)
	}
}

func TestReturnAllPayDayHandleStatusOk(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "http://localhost:8081/howMuch?pay_day=25", nil)
	w := httptest.NewRecorder()
	currentTime := time.Date(2022, time.Month(11), 17, 0, 0, 0, 0, time.UTC)
	handler := NewTimeHandler(currentTime)
	handler.returnAllPayDaysHandle(w, req)
	res := w.Result()
	if res.StatusCode != 200 {
		t.Errorf("expected status code 200, got %v", res.StatusCode)
	}
}

func TestReturnAllPayDayHandleStatusBadRequest(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "http://localhost:8081/listHowMany?pay_day=40", nil)
	w := httptest.NewRecorder()
	currentTime := time.Date(2022, time.Month(11), 17, 0, 0, 0, 0, time.UTC)
	handler := NewTimeHandler(currentTime)
	handler.returnAllPayDaysHandle(w, req)
	res := w.Result()
	if res.StatusCode != 400 {
		t.Errorf("expected status code 400, got %v", res.StatusCode)
	}
}

func TestReturnNextPayDayHandleStatusBadInput(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "http://localhost:8081/howMuch?pay_day=asdf", nil)
	w := httptest.NewRecorder()
	currentTime := time.Date(2022, time.Month(3), 17, 0, 0, 0, 0, time.UTC)
	handler := NewTimeHandler(currentTime)
	handler.returnNextPayDayHandle(w, req)
	res := w.Result()
	if res.StatusCode != 400 {
		t.Errorf("expected status code 400 got %v", res.StatusCode)
	}
}

func TestReturnAllPayDayHandleStatusBadInput(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "http://localhost:8081/listHowMany?pay_day=asdf", nil)
	w := httptest.NewRecorder()
	currentTime := time.Date(2022, time.Month(11), 17, 0, 0, 0, 0, time.UTC)
	handler := NewTimeHandler(currentTime)
	handler.returnAllPayDaysHandle(w, req)
	res := w.Result()
	if res.StatusCode != 400 {
		t.Errorf("expected status code 400, got %v", res.StatusCode)
	}
}
