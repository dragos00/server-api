package apiserver

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"serverApp/paydate"
	"strconv"
	"time"
)

// Start function starts the server on the port indicated by address string
// It is using a TimeHandler to handle requests related to time
func Start(address string, handler TimeHandler) {
	handleRequests(handler)
	log.Print(http.ListenAndServe(address, nil))
}

func handleRequests(handler TimeHandler) {
	http.HandleFunc("/howMuch", handler.returnNextPayDayHandle)
	http.HandleFunc("/listHowMany", handler.returnAllPayDaysHandle)
}

func NewTimeHandler(t time.Time) TimeHandler {
	return TimeHandler{t}
}

type TimeHandler struct {
	currentTime time.Time
}

func (c TimeHandler) returnNextPayDayHandle(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only GET request allowed", 403)
		log.Print(errors.New("not a GET request"))
		return
	}
	payDayString := r.URL.Query().Get("pay_day")
	payDay, err := strconv.Atoi(payDayString)
	if err != nil {
		http.Error(w, "invalid input", 400)
		log.Print(err.Error())
		return
	}
	numberOfDays, err := paydate.CalculatePayDay(payDay, c.currentTime)
	if err != nil {
		http.Error(w, "invalid input", 400)
		log.Print(err.Error())
		return
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(numberOfDays)
	if err != nil {
		http.Error(w, "body parse error", 400)
		log.Print(err.Error())
	}
}

func (c TimeHandler) returnAllPayDaysHandle(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "only GET request allowed", 403)
		log.Print(errors.New("not a GET request"))
		return
	}
	payDayString := r.URL.Query().Get("pay_day")
	payDay, err := strconv.Atoi(payDayString)
	if err != nil {
		http.Error(w, "invalid input", 400)
		log.Print(err.Error())
		return
	}
	payDateList, err := paydate.CalculatePayDayEveryMonth(payDay, c.currentTime)
	if err != nil {
		http.Error(w, "invalid input", 400)
		log.Print(err.Error())
		return
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(payDateList)
	if err != nil {
		http.Error(w, "body parse error", 400)
		log.Print(err)
	}
}
