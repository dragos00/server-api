package paydate

import (
	"errors"
	"time"
)

type DaysRemaining struct {
	Days int `json:"days_remaining"`
}

type YearlyPayDates struct {
	Month int `json:"month"`
	Days  int `json:"days_remaining"`
}

// CalculatePayDay takes the pay date and returns the remaining days
// between pay day and current day. Error can be only from converting
// string to int. If pay day is in weekend it will change the pay
// day to friday
func CalculatePayDay(payDay int, currentDay time.Time) (DaysRemaining, error) {
	var daysRemaining DaysRemaining
	if payDay < 1 || payDay > 31 {
		return daysRemaining, errors.New("days outside 1-31 range")
	}
	var payDate time.Time
	if payDay > currentDay.Day() {
		payDate = date(2022, int(currentDay.Month()), payDay)
	} else {
		payDate = date(2022, int(currentDay.Month()+1), payDay)
	}
	payDate = changeFromWeekendToFriday(payDate)
	daysRemaining.Days = int(payDate.Sub(currentDay).Hours() / 24)
	return daysRemaining, nil
}

// CalculatePayDayEveryMonth takes the pay date and returns the remaining
// days between current day and pay day in every month. The output will
// be an YearlyPayDates structure containing month and remaining days
// Error can be only from converting string to int
func CalculatePayDayEveryMonth(payDay int, currentDay time.Time) ([]YearlyPayDates, error) {
	if payDay < 1 || payDay > 31 {
		return nil, errors.New("days outside 1-31 range")
	}
	monthsLeft := 12 - int(currentDay.Month())
	monthIndex := int(currentDay.Month())
	var payDateList []YearlyPayDates
	var payDateStruct YearlyPayDates
	if payDay > currentDay.Day() {
		monthsLeft++
		monthIndex--
	}
	for {
		if monthsLeft == 0 {
			break
		}
		monthIndex++
		payDate := date(currentDay.Year(), monthIndex, payDay)
		payDate = changeFromWeekendToFriday(payDate)
		payDateStruct.Month = monthIndex
		payDateStruct.Days = int(payDate.Sub(currentDay).Hours() / 24)
		payDateList = append(payDateList, payDateStruct)
		monthsLeft--
	}
	return payDateList, nil
}

func changeFromWeekendToFriday(t time.Time) time.Time {
	var payDate time.Time
	switch t.Weekday() {
	case time.Saturday:
		payDate = date(t.Year(), int(t.Month()), t.Day()-1)
		return payDate
	case time.Sunday:
		payDate = date(t.Year(), int(t.Month()), t.Day()-2)
		return payDate
	}
	return t
}

func date(year, month, day int) time.Time {
	return time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC)
}
