package paydate

import (
	"reflect"
	"testing"
	"time"
)

var (
	testData1 = YearlyPayDates{
		Month: 11,
		Days:  8,
	}
	testData2 = YearlyPayDates{
		Month: 12,
		Days:  36,
	}
)

func TestCalculatePayDay(t *testing.T) {
	cTime := time.Date(2022, time.Month(3), 17, 0, 0, 0, 0, time.UTC)
	remainingDays, _ := CalculatePayDay(25, cTime)
	if remainingDays.Days != 8 {
		t.Errorf("expected 8, got %v", remainingDays)
	}
}

func TestCalculatePayDayNoError(t *testing.T) {
	cTime := time.Date(2022, time.Month(3), 17, 0, 0, 0, 0, time.UTC)
	_, err := CalculatePayDay(25, cTime)
	if err != nil {
		t.Errorf("CaclculatePayDay error: %v", err)
	}
}

func TestCalculatePayDayError(t *testing.T) {
	cTime := time.Date(2022, time.Month(3), 17, 0, 0, 0, 0, time.UTC)
	_, err := CalculatePayDay(40, cTime)
	if err == nil {
		t.Errorf("CalculatePayDay should throw error")
	}
}

func TestCalculatePayDayEveryMonth(t *testing.T) {
	cTime := time.Date(2022, time.Month(11), 17, 0, 0, 0, 0, time.UTC)
	got, _ := CalculatePayDayEveryMonth(25, cTime)
	var testData []YearlyPayDates
	testData = append(testData, testData1, testData2)
	if !reflect.DeepEqual(got, testData) {
		t.Errorf("got %v", testData)
	}
}

func TestCalculatePayDayEveryMonthError(t *testing.T) {
	cTime := time.Date(2022, time.Month(11), 17, 0, 0, 0, 0, time.UTC)
	_, err := CalculatePayDayEveryMonth(40, cTime)
	if err == nil {
		t.Errorf("CalculatePayDayEveryMonth should throw error")
	}
}

func TestCalculatePayDayEveryMonthNoError(t *testing.T) {
	cTime := time.Date(2022, time.Month(11), 17, 0, 0, 0, 0, time.UTC)
	_, err := CalculatePayDayEveryMonth(25, cTime)
	if err != nil {
		t.Errorf("CalculatePayDayEveryMonth error: %v", err)
	}
}
