package main

import (
	"serverApp/apiserver"
	"time"
)

func main() {
	t := apiserver.NewTimeHandler(time.Now())
	apiserver.Start(":8081", t)
}
